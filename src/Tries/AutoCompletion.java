package Tries;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;



public class AutoCompletion {
    public static ArrayList<String> readFile(String fileName){
        //méthode pour extraire des fichiers de type data**** (contenant des requêtes de moteur de recherche, mais avec d'autres informations inutiles ici)
        //la liste des chaînes correspondant aux recherches
        ArrayList<String> res = new ArrayList<>();
        try{

            Scanner sc = new Scanner(new File(fileName));
            sc.useDelimiter(",");
            while(sc.hasNext()){
                String s = sc.next();

                if(s.length()>=4){
                    if(s.substring(1,4).equals("key")) {
                        Scanner sc2 = new Scanner(s);
                        sc2.useDelimiter(":");
                        sc2.next();//consomme le "keyphrase"
                        String s2 = sc2.next();
                        res.add(s2.substring(1,s2.length()-1));
                    }
                }
            }

        }
        catch(FileNotFoundException e){
            System.out.println("Problème dans readFile : " + e.getMessage());
        }
        return res;

    }

    public static Ptree<Chain> buildPtreeV0(ArrayList<String> dico){
        //action : retourne un Ptree contenant toutes les chaînes de dico
        Ptree<Chain> res = new Ptree<>();
        for(String mot: dico){
            res.add(new Chain(mot));
        }
        return res;
    }

    public static Ptree<Chain> buildPtreeV1(ArrayList<String> dico){
        //action : retourne un Ptree a contenant toutes les chaînes de dico et tel que les attributs
        //listeFilsNonVides de a sont à jour
        Ptree<Chain> dictioTrie = buildPtreeV0(dico);
        dictioTrie.remplirFilsNonVides();
        return dictioTrie;
    }

    public static Ptree<Chain> buildPtreeV2(ArrayList<String> dico){
        //action : retourne un Ptree a contenant toutes les chaînes de dico et tel que les attributs
        //listeFilsNonVides et raccourcis de a sont à jour
        Ptree<Chain> dictioTrie = buildPtreeV1(dico);
        dictioTrie.remplirRaccourcis();
        //dictioTrie.remplirPrefD(new ArrayList<>()); //utile si l'on voulait remplir les attributs "prefixes" du Ptree (pour debug seulement)
        return dictioTrie;
    }



    public static long autoCompleteNaif(ArrayList<String> dico, int n, int taillePattern){
        //prérequis : n < dico.size()
        //action : exécute recherche(dico,s) pour les n chaines s construites de la façon suivante :
        // si par exemple dico = ["bonjour", "bille", "maisons", "squash"], n=3 et taillePattern = 2,
        // alors on construira s = "bo","bi", et "ma"
        //retourne le temps passé à faire ces n recherches
        Ptree<Chain> dictioTrie = buildPtreeV0(dico);
        long start = System.currentTimeMillis();
        for(int i=0;i<n;i++) {
            String s = dico.get(i).substring(0, Math.min(taillePattern, dico.get(i).length()));
            ArrayList<Integer> prefix = new Chain(s).getKey();
            recherche(dico,s);
        }
        long stop = System.currentTimeMillis();
        long duree = stop-start;
        return duree;
    }



    public static ArrayList<String> recherche(ArrayList<String> dico, String s){
        ArrayList<String> res = new ArrayList<>();
        int slength = s.length();
        for(String c : dico) {
            boolean ok = true;
            int i=0;

            while(ok && i<Math.min(slength,c.length())){
                ok=s.charAt(i)==c.charAt(i);
                i++;
            }
            if(ok && i==slength)
                res.add(c);
        }
        return res;
    }


    public static long autoCompleteV0(ArrayList<String> dico, int n, int taillePattern){
        //prérequis : n < dico.size()
        //action : construit un Ptree avec buildPtreeV0, puis exécute lsiteALLV0(p) pour les n clefs p construites de la façon suivante :
        // si par exemple dico = ["bonjour", "bille", "maisons", "squash"], n=3 et taillePattern = 2,
        // alors on construira p = "bo".getKey(),"bi".getKey(), et "ma".getKey()
        //retourne le temps passé à faire ces n appels
        Ptree<Chain> dictioTrie = buildPtreeV0(dico);
        long start = System.currentTimeMillis();
        for(int i=0;i<n;i++) {
            String s = dico.get(i).substring(0, Math.min(taillePattern, dico.get(i).length()));
            ArrayList<Integer> prefix = new Chain(s).getKey();
            dictioTrie.listAllV0(prefix);
        }
        long stop = System.currentTimeMillis();
        long duree = stop-start;
        return duree;
    }

    public static long autoCompleteV1(ArrayList<String> dico, int n, int taillePattern){
        //prérequis : n < dico.size()
        //action : construit un Ptree avec buildPtreeV1, puis exécute lsiteALLV1(p) pour les n clefs p construites de la façon suivante :
        // si par exemple dico = ["bonjour", "bille", "maisons", "squash"], n=3 et taillePattern = 2,
        // alors on construira p = "bo".getKey(),"bi".getKey(), et "ma".getKey()
        //retourne le temps passé à faire ces n appels

        Ptree<Chain> dictioTrie = buildPtreeV1(dico);
        long start = System.currentTimeMillis();
        for(int i=0;i<n;i++) {
            String s = dico.get(i).substring(0, Math.min(taillePattern, dico.get(i).length()));
            ArrayList<Integer> prefix = new Chain(s).getKey();
            dictioTrie.listAllV1(prefix);
        }
        long stop = System.currentTimeMillis();
        long duree = stop-start;
        return duree;
    }

    public static long autoCompleteV2(ArrayList<String> dico, int n, int taillePattern){
        //prérequis : n < dico.size()
        //action : construit un Ptree avec buildPtreeV2, puis exécute lsiteALLv2(p) pour les n clefs p construites de la façon suivante :
        // si par exemple dico = ["bonjour", "bille", "maisons", "squash"], n=3 et taillePattern = 2,
        // alors on construira p = "bo".getKey(),"bi".getKey(), et "ma".getKey()
        //retourne le temps passé à faire ces n appels

        Ptree<Chain> dictioTrie = buildPtreeV2(dico);
        long start = System.currentTimeMillis();
        for(int i=0;i<n;i++) {
            String s = dico.get(i).substring(0, Math.min(taillePattern, dico.get(i).length()));
            ArrayList<Integer> prefix = new Chain(s).getKey();
            ArrayList<Integer> prefixback = new ArrayList<>(prefix);
            dictioTrie.listAllV2(prefix);// + "vs" + dictioTrie.listAllV2int(prefixback));

        }
        long stop = System.currentTimeMillis();
        long duree = stop-start;
        return duree;
    }

    /* test en itératif, juste pour voir si ça gagnait qq chose
    public static long autoCompleteV2Ite(ArrayList<String> dictioRequests, int n, int taillePattern){
        Ptree<Chain> dictioTrie = buildPtreeV2(dictioRequests);
        long start = System.currentTimeMillis();
        for(int i=0;i<n;i++) {
            String s = dictioRequests.get(i).substring(0, Math.min(taillePattern, dictioRequests.get(i).length()));
            ArrayList<Integer> prefix = new Chain(s).getKey();
            dictioTrie.listAllV2Ite(prefix);
        }
        long stop = System.currentTimeMillis();
        long duree = stop-start;
        return duree;
    }
    */



    public static int sommeSizes(ArrayList<String> l){
        int res = 0;
        for(String s : l){
            res+=s.length();
        }
        return res;
    }



    public static void main(String [] args){
        ArrayList<String> dictio = readFile("data10000");

        long tnaif = autoCompleteNaif(dictio,30,2);
        long t0 = autoCompleteV0(dictio,30,2);
        long t1 = autoCompleteV1(dictio,30,2);
        long t2 = autoCompleteV2(dictio,30,2);
        //long t2ite = autoCompleteV2Ite(dictio,1,0);

        System.out.println("naif " + tnaif);
        System.out.println("t0  " + t0);
        System.out.println("t1  " + t1);
        System.out.println("t2  " + t2);
        //System.out.println("t2ite  " + t2ite);




        //partie du main juste pour générer un petit arbre en svg
        /*
        Person a1 = new Person(2);
        Person a2 = new Person(14);
        Person a3 = new Person(18);
        Person a4 = new Person(51);
        Person a5 = new Person(5);


        Ptree<Person> ta = new Ptree<>();
        ta.add(a1);
        ta.add(a2);
        ta.add(a3);
        ta.add(a4);
        ta.add(a5);

        ta.toSVG(true).saveAsFile("taSimple.svg");
        */
    }
}
