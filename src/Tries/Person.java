package Tries;

import java.util.Objects;

public class Person implements ItemWithKey {
    private int numSecu;

    //les clefs sont basées sur le numSecu ainsi :
    // si le numSecu est 312, alors
    // getKeyLength() = 3,
    // getDigit(0)=3
    // getDigit(1)=1
    // getDigit(2)=2


    public Person(int a){
        this.numSecu =a;
    }

    @Override
    public int getKeyLength() {
        int x=10;
        int res = 1;
        while(numSecu /x > 0){
            x=10*x;
            res++;
        }
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person p = (Person) o;
        return numSecu == p.numSecu;
    }

    @Override
    public int hashCode() {
        return Objects.hash(numSecu);
    }

    public int getBase(){
        return 10;
    }

    public int getDigit(int i){
        int p = (int) Math.pow(10,getKeyLength()-i-1);
        return (numSecu /p)%10;
    }
    public Person getData(){
        return this;
    }

    @Override
    public String toString() {
        return "Personne{" +"numSecu=" + numSecu +'}';
    }
}
